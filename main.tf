variable "project_id" {
  type = string
  default = "terraform-test-307911"
}

variable "region" {
  type = string
  default = "europe-west1"
}

variable "credentials_file" {
  type = string
}

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.55.0"
    }
  }
}

provider "google" {
  credentials = file(var.credentials_file)
  project = var.project_id
  region  = var.region
}

resource "google_project_iam_member" "a_editor" {
  project = var.project_id
  role = "roles/editor"
  member = "serviceAccount:service-a@terraform-test-307911.iam.gserviceaccount.com"
}
